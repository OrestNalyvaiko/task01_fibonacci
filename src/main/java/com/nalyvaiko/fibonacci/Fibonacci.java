package com.nalyvaiko.fibonacci;

/**
 * Fibonacci class designed for building Fibonacci numbers,
 * calculate percentage of odd and even numbers.
 *
 * @author Orest Nalyvaiko
 */
public class Fibonacci {


  /**
   * Integer final variable store percentage.
   */
  private static final int PERCENTAGE = 100;
  /**
   * Integer variable store size of Fibonacci array.
   */
  private int sizeOfSet;
  /**
   * Integer array variable store array of Fibonacci numbers.
   */
  private int[] fibonacciNumbers;
  /**
   * Double variable store percentage of odd numbers.
   */
  private double percentageOfOddNumbers;
  /**
   * Double variable store percentage of even numbers.
   */
  private double percentageOfEvenNumbers;

  /**
   * Fibonacci constructor designed for initialization.
   *
   * @param size size of set
   * @param biggestOddNumber the biggest odd number for first Fibonacci number
   * @param biggestEvenNumber the biggest even number
   * for second Fibonacci number
   */
  public Fibonacci(final int size, final int biggestOddNumber,
        final int biggestEvenNumber) {
    this.sizeOfSet = size;
    fibonacciNumbers = new int[this.sizeOfSet];
    fibonacciNumbers[0] = biggestOddNumber;
    fibonacciNumbers[1] = biggestEvenNumber;
    buildFibonacciNumbers();
    calculatePercentageOfOddAndEvenNumbers();
  }

  /**
   * Method calculate percentage of odd and even numbers.
   */
  private void calculatePercentageOfOddAndEvenNumbers() {
    int countOfOddNumbers = 0;
    int countOfEvenNumbers = 0;
    for (int number : fibonacciNumbers) {
      if (number % 2 == 0) {
        countOfEvenNumbers++;
      } else {
        countOfOddNumbers++;
      }
    }
    percentageOfOddNumbers =
        (double) countOfOddNumbers / sizeOfSet * PERCENTAGE;
    percentageOfEvenNumbers =
        (double) countOfEvenNumbers / sizeOfSet * PERCENTAGE;
  }

  /**
   * Method build Fibonacci numbers according to the size of set,first number -
   * the biggest odd number, the second - the biggest even number.
   */
  private void buildFibonacciNumbers() {
    for (int i = 2; i < sizeOfSet; i++) {
      fibonacciNumbers[i] = fibonacciNumbers[i - 2] + fibonacciNumbers[i - 1];
    }
  }

  /**
   * Simple getter.
   *
   * @return array of Fibonacci numbers
   */
  public final int[] getFibonacciNumbers() {
    return fibonacciNumbers;
  }

  /**
   * Simple getter.
   *
   * @return percentage of odd numbers in array of Fibonacci numbers
   */
  public final double getPercentageOfOddNumbers() {
    return percentageOfOddNumbers;
  }

  /**
   * Simple getter.
   *
   * @return percentage of even numbers in array of Fibonacci numbers
   */
  public final double getPercentageOfEvenNumbers() {
    return percentageOfEvenNumbers;
  }
}
