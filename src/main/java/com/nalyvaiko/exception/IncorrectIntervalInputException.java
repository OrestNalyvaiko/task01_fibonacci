package com.nalyvaiko.exception;

/**
 * This is class of exception produced by incorrect interval input.
 *
 * @author Orest Nalyvaiko
 */
public class IncorrectIntervalInputException extends Exception{

    /**
     * Construct with the specified detail message.
     *
     * @param message detail message
     */
    public IncorrectIntervalInputException(String message){
        super(message);
    }
}
