package com.nalyvaiko.exception;

/**
 * This is class of exception produced by incorrect size of set.
 *
 * @author Orest Nalyvaiko
 */
public class IncorrectSizeOfSetException extends Exception {

    /**
     * Construct with the specified detail message.
     *
     * @param message detail message
     */
    public IncorrectSizeOfSetException(String message){
        super(message);
    }
}
