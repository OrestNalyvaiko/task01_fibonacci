package com.nalyvaiko.interval;

import java.io.*;

public class IntervalFileWriter implements AutoCloseable {

    private BufferedWriter bufferedWriter;
    private OutputStreamWriter outputStreamWriter;

    public IntervalFileWriter(FileOutputStream fileOutputStream) {
        outputStreamWriter = new OutputStreamWriter(fileOutputStream);
        bufferedWriter = new BufferedWriter(outputStreamWriter);
    }

    public void writeInterval(int startOfTheInterval, int endOfTheInterval) {
        try {
            bufferedWriter.write("The interval " + startOfTheInterval + ", "
                    + endOfTheInterval);
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
    }

    @Override
    public void close() throws IOException {
        bufferedWriter.close();
        outputStreamWriter.close();
    }
}
