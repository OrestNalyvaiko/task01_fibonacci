package com.nalyvaiko.interval;

/**
 * Interval class designed for finding even numbers from end to start,
 * odd numbers from start to end,
 * calculating the sum of odd and even numbers.
 *
 * @author Orest Nalyvaiko
 */
public class Interval {

  /**
   * Integer variable store start of the interval.
   */
  private int startTheInterval;
  /**
   * Integer variable store end of the interval.
   */
  private int endTheInterval;
  /**
   * Integer variable store sum of odd numbers.
   */
  private int sumOfOddNumbers;
  /**
   * Integer variable store sum of even numbers.
   */
  private int sumOfEvenNumbers;
  /**
   * Integer variable store the biggest odd number.
   */
  private int biggestOddNumber;
  /**
   * Integer variable store the biggest even number.
   */
  private int biggestEvenNumber;
  /**
   * Integer array variable store array of odd numbers from start to end
   * of the interval.
   */
  private int[] oddNumbersFromStartToEnd;
  /**
   * Integer array variable store array of even numbers from end to start
   * of the interval.
   */
  private int[] evenNumbersFromEndToStart;
  /**
   * Integer variable store the count of odd numbers.
   */
  private int countOfOddNumbers;
  /**
   * Integer variable store the count of even numbers.
   */
  private int countOfEvenNumbers;

  /**
   * Interval constructor designed for initialization.
   *
   * @param start start of the interval
   * @param end end of the interval
   */
  public Interval(final int start, final int end) {
    this.startTheInterval = start;
    this.endTheInterval = end;
    getCountOfEvenAndOddNumbers();
    findOddNumbersFromStartToEnd();
    findEvenNumbersFromEndToStart();
    calculateSumOfOddNumbers();
    calculateSumOfEvenNumbers();
    biggestEvenNumber = evenNumbersFromEndToStart[0];
    biggestOddNumber = oddNumbersFromStartToEnd[countOfOddNumbers - 1];
  }

  /**
   * Method find even numbers from end to start of the interval.
   */
  private void findEvenNumbersFromEndToStart() {
    evenNumbersFromEndToStart = new int[countOfEvenNumbers];
    int index = 0;
    for (int number = endTheInterval; number >= startTheInterval; number--) {
      if (number % 2 == 0) {
        evenNumbersFromEndToStart[index] = number;
        index++;
      }
    }
  }

  /**
   * Method find odd numbers from start to end of the interval.
   */
  private void findOddNumbersFromStartToEnd() {
    oddNumbersFromStartToEnd = new int[countOfOddNumbers];
    int index = 0;
    for (int number = startTheInterval; number <= endTheInterval; number++) {
      if (number % 2 == 1) {
        oddNumbersFromStartToEnd[index] = number;
        index++;
      }
    }
  }

  /**
   * Method get count of even and odd numbers.
   */
  private void getCountOfEvenAndOddNumbers() {
    for (int i = startTheInterval; i <= endTheInterval; i++) {
      if (i % 2 == 0) {
        countOfEvenNumbers++;
      } else {
        countOfOddNumbers++;
      }
    }
  }

  /**
   * Method calculate the sum of even numbers.
   */
  private void calculateSumOfEvenNumbers() {
    for (int number : evenNumbersFromEndToStart) {
      sumOfEvenNumbers += number;
    }
  }

  /**
   * Method calculate the sum of odd numbers.
   */
  private void calculateSumOfOddNumbers() {
    for (int number : oddNumbersFromStartToEnd) {
      sumOfOddNumbers += number;
    }
  }

  /**
   * Getter.
   *
   * @return array of even numbers from end to start
   */
  public final int[] getEvenNumbersFromEndToStart() {
    return evenNumbersFromEndToStart;
  }

  /**
   * Getter.
   *
   * @return array of odd numbers from start to end
   */
  public final int[] getOddNumbersFromStartToEnd() {
    return oddNumbersFromStartToEnd;
  }

  /**
   * Getter.
   *
   * @return the sum of even numbers
   */
  public final int getSumOfEvenNumbers() {
    return sumOfEvenNumbers;
  }

  /**
   * Getter.
   *
   * @return the sum of odd numbers
   */
  public final int getSumOfOddNumbers() {
    return sumOfOddNumbers;
  }

  /**
   * Getter.
   *
   * @return the biggest odd number
   */
  public final int getBiggestOddNumber() {
    return biggestOddNumber;
  }

  /**
   * Getter.
   *
   * @return the biggest even number
   */
  public final int getBiggestEvenNumber() {
    return biggestEvenNumber;
  }

  public int getStartTheInterval() {
    return startTheInterval;
  }

  public int getEndTheInterval() {
    return endTheInterval;
  }
}
