package com.nalyvaiko.userservice;

import com.nalyvaiko.exception.IncorrectIntervalInputException;
import com.nalyvaiko.exception.IncorrectSizeOfSetException;
import com.nalyvaiko.fibonacci.Fibonacci;
import com.nalyvaiko.interval.Interval;
import com.nalyvaiko.interval.IntervalFileWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * UserService class designed for service user.
 *
 * @author Orest Nalyvaiko
 */
public class UserService {

    /**
     * Integer final variable for denote end of the interval.
     */
    private static final int END_OF_THE_INTERVAL = 100;
    /**
     * Interval object designed for getting information about interval numbers.
     */
    private Interval interval;
    /**
     * Fibonacci object designed for getting information about Fibonacci
     * numbers.
     */
    private Fibonacci fibonacci;

    /**
     * Method designed for entering the interval.
     */
    public final void enterTheInterval() {
        System.out.println("Enter the interval");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the start of the interval: ");
        int startOfTheInterval = scanner.nextInt();
        System.out.print("Enter the end of the interval: ");
        int endOfTheInterval = scanner.nextInt();
        try {
            checkInputIntervals(startOfTheInterval, endOfTheInterval);
            interval = new Interval(startOfTheInterval, endOfTheInterval);
            writeInterval();
        } catch (IncorrectIntervalInputException exception) {
            System.out.println(exception.getMessage());
            enterTheInterval();
        }
    }

    /**
     * Method print choices for operating with interval numbers.
     */
    public final void printIntervalChoices() {
        System.out.println("1.Enter new interval");
        System.out.println("2.Print odd numbers from start to end");
        System.out.println("3.Print even numbers from end to start");
        System.out.println("4.Print the sum of odd numbers");
        System.out.println("5.Print the sum of even numbers");
        System.out.println("6.Enter to work with Fibonacci numbers");
        System.out.println("7.Exit");
        System.out.print("Enter your choice: ");
    }

    /**
     * Method print odd numbers from start to end of the interval.
     */
    public final void printOddNumbersFromStartToEnd() {
        System.out.print("Odd numbers from start to end: ");
        for (int number : interval.getOddNumbersFromStartToEnd()) {
            System.out.print(number + " ");
        }
        System.out.println();
    }

    /**
     * Method print even numbers from end to start of the interval.
     */
    public final void printEvenNumbersFromEndToStart() {
        System.out.print("Even numbers from end to start: ");
        for (int number : interval.getEvenNumbersFromEndToStart()) {
            System.out.print(number + " ");
        }
        System.out.println();
    }

    /**
     * Method print the sum of odd numbers.
     */
    public final void printSumOfOddNumbers() {
        System.out.print("The sum of odd numbers: "
                + interval.getSumOfOddNumbers() + "\n");
    }

    /**
     * Method print the sum of even numbers.
     */
    public final void printSumOfEvenNumbers() {
        System.out.print("The sum of even numbers: "
                + interval.getSumOfEvenNumbers() + "\n");
    }

    /**
     * Method designed for entering size of set.
     */
    public final void enterSizeOfSet() {
        System.out.print("Enter size of set: ");
        Scanner scanner = new Scanner(System.in);
        int sizeOfSet = scanner.nextInt();
        try {
            checkSizeOfSet(sizeOfSet);
            fibonacci = new Fibonacci(sizeOfSet, interval.getBiggestOddNumber(),
                    interval.getBiggestEvenNumber());
        } catch (IncorrectSizeOfSetException exception) {
            System.out.println(exception.getMessage());
            enterSizeOfSet();
        }
    }

    /**
     * Method print choices for operating with Fibonacci numbers.
     */
    public final void printFibonacciChoices() {
        System.out.println("1.Print Fibonacci numbers");
        System.out.println("2.Print percentage of odd Fibonacci numbers");
        System.out.println("3.Print percentage of even Fibonacci numbers");
        System.out.println("4.Exit");
        System.out.print("Enter your choice: ");
    }

    /**
     * Method print Fibonacci numbers.
     */
    public final void printFibonacciNumbers() {
        System.out.println(
                "Fibonacci numbers, where F1 - the biggest odd number and "
                        + "F2 – the biggest even number: ");
        for (int number : fibonacci.getFibonacciNumbers()) {
            System.out.print(number + " ");
        }
        System.out.println();
    }

    /**
     * Method print the percentage of odd Fibonacci numbers.
     */
    public final void printPercentageOfOddNumbers() {
        System.out.println("Percentage of odd Fibonacci numbers: " + fibonacci
                .getPercentageOfOddNumbers());
    }

    /**
     * Method print the percentage of even Fibonacci numbers.
     */
    public final void printPercentageOfEvenNumbers() {
        System.out.println("Percentage of even Fibonacci numbers: " + fibonacci
                .getPercentageOfEvenNumbers());
    }

    /**
     * Method check the interval input.
     *
     * @param startOfTheInterval start of the interval
     * @param endOfTheInterval   end of the interval
     * @throws IncorrectIntervalInputException if the interval inputs are
     *                                         incorrect
     */
    private void checkInputIntervals(int startOfTheInterval,
                                     int endOfTheInterval)
            throws IncorrectIntervalInputException {
        if (startOfTheInterval < 1
                || startOfTheInterval > END_OF_THE_INTERVAL
                || endOfTheInterval < 1
                || endOfTheInterval > END_OF_THE_INTERVAL) {
            throw new IncorrectIntervalInputException("The interval must " +
                    "be within range 1 and 100");
        } else {
            if (startOfTheInterval > endOfTheInterval) {
                throw new IncorrectIntervalInputException
                        ("Start of the interval must be less than the end");
            }
        }
    }

    /**
     * Method check the size of set.
     *
     * @param sizeOfSet the size of set
     * @throws IncorrectSizeOfSetException if the size of set are less than 2
     */
    private void checkSizeOfSet(int sizeOfSet)
            throws IncorrectSizeOfSetException {
        if (sizeOfSet < 2) {
            throw new IncorrectSizeOfSetException("Size must be " +
                    "greater than 1");
        }
    }

    private void writeInterval() {
        try (IntervalFileWriter intervalFileWriter = new IntervalFileWriter(
                new FileOutputStream("information.txt"))) {
            intervalFileWriter.writeInterval(interval.getStartTheInterval()
                    , interval.getEndTheInterval());
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
