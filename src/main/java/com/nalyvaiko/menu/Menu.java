package com.nalyvaiko.menu;

import com.nalyvaiko.userservice.UserService;
import java.util.Scanner;

/**
 * Menu class designed for display menu for user.
 *
 * @author Orest Nalyvaiko
 */
public class Menu {

  /**
   * UserService object designed for service user,display information.
   */
  private UserService userService;

  /**
   * Menu constructor designed for initialization.
   */
  public Menu() {
    userService = new UserService();
  }

  /**
   * Method display menu for user.
   */
  public final void start() {
    userService.enterTheInterval();
    while (true) {
      userService.printIntervalChoices();
      Scanner scanner = new Scanner(System.in);
      int choice = scanner.nextInt();
      switch (choice) {
        case 1:
          start();
          break;
        case 2:
          userService.printOddNumbersFromStartToEnd();
          break;
        case 3:
          userService.printEvenNumbersFromEndToStart();
          break;
        case 4:
          userService.printSumOfOddNumbers();
          break;
        case 5:
          userService.printSumOfEvenNumbers();
          break;
        case 6:
          workWithFibonacci();
          break;
        case 7:
          System.exit(0);
          break;
        default:
          break;
      }
    }
  }

  /**
   * Method display menu for working with Fibonacci numbers.
   */
  private void workWithFibonacci() {
    userService.enterSizeOfSet();
    boolean state = true;
    while (state) {
      userService.printFibonacciChoices();
      Scanner scanner = new Scanner(System.in);
      int choice = scanner.nextInt();
      switch (choice) {
        case 1:
          userService.printFibonacciNumbers();
          break;
        case 2:
          userService.printPercentageOfOddNumbers();
          break;
        case 3:
          userService.printPercentageOfEvenNumbers();
          break;
        case 4:
          state = false;
        default:
          break;
      }
    }
  }
}
