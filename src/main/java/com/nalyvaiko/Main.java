package com.nalyvaiko;

import com.nalyvaiko.menu.Menu;

/**
 * Main class designed for starting the program.
 *
 * @author Orest Nalyvaiko
 */
public final class Main {

  /**
   * Private constructor.
   */
  private Main() {

  }

  /**
   * Method is the entry point of the program.
   *
   * @param args args from the console
   */
  public static void main(final String[] args) {
    Menu menu = new Menu();
    menu.start();
  }
}
